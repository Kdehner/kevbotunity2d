﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {
    int max = 1000;
    int min = 1;

    Debug.Log("Welcome to number wizard!");
    Debug.Log("Please think of a number!");
    Debug.Log("The highest number you can pick is: " + max);
    Debug.Log("The lowst number you can pick is: " + min);
    Debug.Log("Tell me, is your number higher or lower than 500?");
    Debug.Log("UP = Higher, DOWN = Lower, ENTER = Correct");

  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.UpArrow))
    {
      Debug.Log("Up Arrow key was pressed.");
    }
    if (Input.GetKeyDown(KeyCode.DownArrow))
    {
      Debug.Log("Down Arrow key was pressed.");
    }
    if (Input.GetKeyDown(KeyCode.Return))
    {
      Debug.Log("Return key was pressed.");
    }
  }
}
